import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoffeeTableComponent } from './CoffeeTable/coffee-table/coffee-table.component';
import { LoginComponent } from './Login/login/login.component';
import { SignupComponent } from './Signup/signup/signup.component';

const routes: Routes = [
  { path: '',   redirectTo: '/Home-component', pathMatch: 'full' },
  { path: 'Home-component', component: CoffeeTableComponent },
  { path: 'login.component', component: LoginComponent },
  { path: 'signup.component', component: SignupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
