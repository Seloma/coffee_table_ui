import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registerForm = this.formBuilder.group({
    firstName: '',
    lastName: '',
    birthdayDate: '',
    emailAddress: '',
    phoneNumber: '',
    inlineRadioOptions:''
  });
  
  constructor(private formBuilder: FormBuilder) { }
  onSubmit(): void {
    // Process checkout data here
    console.warn('Your order has been submitted', this.registerForm.value);
    this.registerForm.reset();
  }
  ngOnInit(): void {
  }

}
